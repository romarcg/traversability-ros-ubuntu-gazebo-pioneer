# Data generation for traversability using Pioneer 3at simulation

This docker image contains code for simulating:

- heightmaps from grayscale images
- pioneer 3at platform with custom wheels and controller

and scripts for simulating random trajectories & retrieving data.

Docker image was generated to be used with nvidia-docker. The docker file is provided in case other settings are needed.

## Run docker container

> This image was generated to be used with nvidia-docker.

To run the container:

1. `sudo -b nohup nvidia-docker-plugin > /tmp/nvidia-docker.log`
2. `xhost  +`
3. `nvidia-docker run -it --env="DISPLAY" --env="QT_X11_NO_MITSHM=1" --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" -v /home/omar/Codes/traversability_docker/traversability-ros-ubuntu-gazebo-pioneer/volume:/volume --name="traversability_gazebo_task" traversability-ros-ubuntu-gazebo-pioneer`

## How-to (inside the container)

The main script is called by a roslaunch file which brings up the simulator, spawns the robot, establishes control parameters, and records the corresponding data to a csv file.

`roslaunch simulate_traversability_core launch_simulations.launch headless:=true gui:=false world_name:=custom1 dataset_type:=training number_tries:=50`

- `world_name` indicates the name of the world file (without extension) which also corresponds to the name of the heightmap image file (without extension)
- `datset_type` is a prefix used to name the generated csv files
- `number_tries` indicates the number of desired trajectories to simulate in the current world

It is suggested to run the simulator `headless` to reduce rendering time/resources. Change headless to `true` and gui to `true` to visualize the simulation while the trajectories are generated.

Resulting `csv` files are stored in `~/dataset_cvs`.

> Remember to run `catkin_make` and `source` before testing the scripts.

## Extra files

Heightmaps and world models are in `src/simulate_traversability/gazebo/models/heightmaps`. If a new heightmap/world is needed place it here.

Gazebo plugins were implemented to extract at each simulation step:
- Pose,
- Twist,
- Contact points,
- and Power

Plugins code is located in `src/simulate_traversability/simulate_traversability_core/src`.

Robot model file is defined in `src/simulate_traversability/description/urdf/pioneer3at.urdf.xacro`.
