#!/usr/bin/env python

#
# It can be run alone:
# rosrun simulate_traversability_core run_simulation.py world_name dataset_type number_tries
#
# Or using the roslaunch file:
#    roslaunch simulate_traversability_core launch_simulations.launch world_name:=custom1 dataset_type:=training number_tries:=50
#
# We can deactivate the gui to speed up the simulation data gathering and 
# make the simulator run headless as such: 
#    roslaunch simulate_traversability_core launch_simulations.launch headless:=true gui:=false world_name:=custom1 dataset_type:=training number_tries:=20

import sys
import rospy
import os
import time
import math
import numpy as np

import pandas as pd

import std_msgs.msg 
from gazebo_msgs.msg import *
from gazebo_msgs.srv import *
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Wrench, PoseStamped, TwistStamped
from nav_msgs.msg import Odometry
from sensor_msgs.msg import JointState
import message_filters
from tf import transformations

from simulate_traversability_core.msg import ContactState
from simulate_traversability_core.msg import Power

## Constant definitions for training/evaluation map simulations
# in m, heightmaps (in simulation) are assumed "squared" (sic) so min_hm_x is equal to -max_hm_x
max_hm_x = 5.0
max_hm_y = 5.0
# max map height, 0 is the min
max_hm_z = 1.0
# in pixels, heightmaps (images) are asusmed squared, gazebo requires sizes 2^n+1 x 2^n+1
im_hm_x = 513
im_hm_y = 513

## For gazebo communication
model_name = "pioneer3at"
root_relative_entity_name = '' # this is the full model and not only the base_link
initial_fixed_twist = Twist()
initial_fixed_twist.linear.x = 0.15 #move forward at .15 m/s
initial_fixed_twist.linear.y = 0
initial_fixed_twist.linear.z = 0
initial_fixed_twist.angular.x = 0
initial_fixed_twist.angular.y = 0
initial_fixed_twist.angular.z = 0

absolute_top_speed = 0.4 #m/s, -0.4 to 0.4

sim_duration = rospy.Duration(20) # 20segs, we could also used non-ros functions with an integer

# For saving the information
output_folder = "~/dataset_cvs/"

def generate_random_pose():
    # x,y (z will be fixed as the max_hm_z so that the robot will drop down), gamma as orientation
    rn = np.random.random_sample((3,))
    random_pose = Pose()
    random_pose.position.x = 2 * max_hm_x *rn[0] - max_hm_x
    random_pose.position.y = 2 * max_hm_y *rn[1] - max_hm_y
    random_pose.position.z = max_hm_z * 0.5 # spawn not at the greatest height to avoid getting upside down when it falls on a tricky obstacle
    qto = transformations.quaternion_from_euler(0, 0, 2*math.pi * rn[1], axes='sxyz')
    random_pose.orientation.x = qto[0]
    random_pose.orientation.y = qto[1]
    random_pose.orientation.z = qto[2]
    random_pose.orientation.w = qto[3]
    return random_pose

def generate_random_model_state():
    random_pose = generate_random_pose()
    model_state = ModelState()
    model_state.model_name = model_name
    model_state.pose = random_pose
    model_state.twist = initial_fixed_twist # is better to set the robot control using a publisher instead of a service that only do it once (very quickly)
    model_state.reference_frame = root_relative_entity_name
    return model_state
def get_model_state():
    rospy.wait_for_service('/gazebo/get_model_state')
    try:
        gms = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
        resp1 = gms(model_name, root_relative_entity_name)
        return resp1
    except rospy.ServiceException, e:
        rospy.logerr("Service call failed: %s"%e)
        return False
def set_model_state(model_state):
    rospy.wait_for_service('/gazebo/set_model_state')
    try:
        gms = rospy.ServiceProxy('/gazebo/set_model_state', SetModelState)
        resp1 = gms(model_state)
        return resp1
    except rospy.ServiceException, e:
        #print "Service call failed: %s"%e
        rospy.logerr("Service call failed: %s"%e)

def usage():
    return "%s [world_name] [number_of_tries]"%sys.argv[0]

## class to manage the publishers and subscirbers to send and recover data from simulation using rostopics instead of services
class PubsSubsManager:
    def __init__(self):
        self.send_cmd_vel_ = False
        #self.process_inputs_ = False
        self.set_flag_process_inputs(False)
        # enable when working with real robot or other platform plugin that publishes odom
        #self.odom_topic_ = "/sim_p3at/odom"
        self.pose_topic_ = "/sim_p3at/pose"
        self.twist_topic_ = "/sim_p3at/twist"
        self.cmd_vel_topic_ = "/sim_p3at/cmd_vel"
        self.joint_states_topic_ = "/sim_p3at/joint_states"
        self.contact_states_topic_ = "/sim_p3at/contact_states"
        self.power_topic_ = "/sim_p3at/power"
        
        #self.odom_sub_ = message_filters.Subscriber(self.odom_topic_, Odometry)
        self.pose_sub_ = message_filters.Subscriber(self.pose_topic_, PoseStamped)
        self.twist_sub_ = message_filters.Subscriber(self.twist_topic_, TwistStamped)
        #self.joint_states_sub_ = message_filters.Subscriber(self.joint_states_topic_, JointState)
        self.contact_states_sub_ = message_filters.Subscriber(self.contact_states_topic_, ContactState)
        self.power_sub_ = message_filters.Subscriber(self.power_topic_, Power)
        
        # add odom when needed
        self.time_synchronizer_ = message_filters.TimeSynchronizer([self.pose_sub_, self.twist_sub_, self.contact_states_sub_, self.power_sub_ ], 100)
        self.time_synchronizer_.registerCallback(self.callback_inputs)
        
        # pubs
        self.pub_cmd_vel_ = rospy.Publisher(self.cmd_vel_topic_, Twist, queue_size=1)
        self.current_twist_cmd_ = initial_fixed_twist
 
        # for storing on csv
        self.world_name_ = "generic_world"
        self.dataset_type_ = "training"
        self.i_sim_ = 0
        self.starting_time_ = rospy.Time.now() #this value is overrriten when explicitly starting a new simulation 
        self.initial_pose_ = 0
        self.data_buffer_ = 0
        
        self.shuffle_delta_time_ = rospy.Duration(30) # every delta_time shufle the control 
        self.start_shuffle_time_ = rospy.Time.now()
        self.counter_callback_calls_ = 0

    def set_flag_cmd_vel(self, val):
        if val == True:
            self.send_cmd_vel_ = val
        else:
            self.send_cmd_vel_ = False

    def set_flag_process_inputs(self, val):
        if val == True:
            self.process_inputs_ = val
        else:
            self.process_inputs_ = False
        rospy.loginfo ("Setting process_inputs flag to: %s", val)

    def get_flag_cmd_vel(self):
        return self.send_cmd_vel_

    def get_flag_process_inputs(self):
        return self.process_inputs_
    
    def publish_cmd_vel(self):
        #h = std_msgs.msg.Header()
        #h.stamp = rospy.Time.now()
        rospy.loginfo ("Publishing cmd_vel message.")
        msg = self.current_twist_cmd_
        self.pub_cmd_vel_.publish(msg)

    def is_pose_in_map(self, pose):
        little_offset = 0.1 # to avoid waiting until the robot is really on the edge
        if pose.position.x > max_hm_x-little_offset or pose.position.x < -max_hm_x+little_offset:
            return False
        if pose.position.y > max_hm_y-little_offset or pose.position.y < -max_hm_y+little_offset:
            return False
        if pose.position.z < -0.03: # beacuse of urdf confituration, the z 0 is ~ -0.02
            return False
        return True

    def toScreenFrame (self, s_x, s_y, s_z):
        # from simulation frame (gazebo) x right, y up, z out of the screen, center is at the middle of the map
        # to x right , y down, ignoring ; xs,ys need to be multiplied by the image size
        xs = s_x + max_hm_x
        ys = -s_y + max_hm_y
        xs = xs/(max_hm_x-(-max_hm_x))
        ys = ys/(max_hm_y-(-max_hm_y))

        return xs, ys
        
    # here we read the pose, twist (ideally from odom) and other topics and store it on a buffer
    def callback_inputs(self, msg_pose, msg_twist, msg_contact_states, msg_power):
        if self.process_inputs_:
            # current time from the stamp of the one message
            current_time = rospy.Time(msg_pose.header.stamp.secs,msg_pose.header.stamp.nsecs)
            # or from now()
            #current_time = rospy.Time.now()
            if self.is_pose_in_map(msg_pose.pose) == False:
                rospy.logwarn("Position out of map, simulation run stopped.")
                self.set_flag_process_inputs(False)
                #self.process_inputs_= False
                return

            if self.counter_callback_calls_ == 0:
                self.starting_time_ = current_time
                rospy.logout("New run started at: %s"%(self.starting_time_))
            self.counter_callback_calls_ = self.counter_callback_calls_ + 1
            
            if self.counter_shuffle_its_ == 0:
                self.start_shuffle_time_ = self.starting_time_
            self.counter_shuffle_its_ = self.counter_shuffle_its_ + 1
                
            if (current_time - self.starting_time_)>= sim_duration:
                #self.process_inputs_= False
                rospy.logout("Simulation run reached duration: (%s - %s = %s)"%(current_time,self.starting_time_,(current_time - self.starting_time_)))
                self.set_flag_process_inputs(False)
                return
            
            # if detal_time is reached, shuffle the control
            if (current_time - self.start_shuffle_time_) > self.shuffle_delta_time_:
                #randomly select a steering angle and a linear vel (0,1) 
                rn_ctrl = np.random.random_sample((2,))
                #self.current_twist_cmd_.linear.x = ((rn_ctrl[0]-0.5)/0.5) * absolute_top_speed # from -top_vel to top_vel
                self.current_twist_cmd_.angular.z = ((rn_ctrl[1]-0.5)/0.5) * math.pi # from -pi to pi
                rospy.logout("Shuffled: new Twist (linear.x, angualr.z): (%f,%f)"%(self.current_twist_cmd_.linear.x,self.current_twist_cmd_.angular.z))
                self.set_flag_cmd_vel(True)
                self.publish_cmd_vel()
                self.counter_shuffle_its_ = 0
                return
                        
            # get the information from gazebo frame
            c_robot_state = ModelState() # get_model_state()
            # store the information from odom
            orientation_euler = transformations.euler_from_quaternion([ msg_pose.pose.orientation.x, msg_pose.pose.orientation.y, msg_pose.pose.orientation.z, msg_pose.pose.orientation.w ], axes='sxyz')
            ii_position = self.toScreenFrame(self.initial_pose_.position.x, self.initial_pose_.position.y, self.initial_pose_.position.z)
            ic_position = self.toScreenFrame(msg_pose.pose.position.x, msg_pose.pose.position.y, msg_pose.pose.position.z)
            
            #self.data_buffer_.loc[len(self.data_buffer_)] = [
            self.data_buffer_np_.append([
                                    (current_time - self.starting_time_).to_sec(),
                                    msg_pose.header.stamp,
                                    ii_position[0]*im_hm_x, 
                                    ii_position[1]*im_hm_y, 
                                    self.initial_pose_.position.x,
                                    self.initial_pose_.position.y,
                                    self.initial_pose_.position.z,
                                    ic_position[0]*im_hm_x,
                                    ic_position[1]*im_hm_y,
                                    msg_pose.pose.position.x, # robot pose position from odom (in this case the plugin bypasses the info from gazebo so it is the same)
                                    msg_pose.pose.position.y,
                                    msg_pose.pose.position.z,
                                    orientation_euler[0], # robot pose orientation from odom (in this case the plugin bypasses the info from gazebo so it is the same)
                                    orientation_euler[1],
                                    orientation_euler[2],
                                    self.current_twist_cmd_.angular.z, # target control inputs steering and linear vel 
                                    self.current_twist_cmd_.linear.x,
                                    msg_twist.twist.linear.x,  # robot's twist from world reference (in this case the plugin bypasses the info from gazebo so it is the same)
                                    msg_twist.twist.linear.y,  # odom has the local frame twist
                                    msg_twist.twist.linear.z,  # enable odom when needed
                                    msg_twist.twist.angular.x,
                                    msg_twist.twist.angular.y,
                                    msg_twist.twist.angular.z,
                                    msg_contact_states.contact[0], # contact point for FLW
                                    msg_contact_states.contact[1], # contact point for FRW
                                    msg_contact_states.contact[2], # contact point for BLW
                                    msg_contact_states.contact[3], # contact point for BRW
                                    msg_contact_states.contact[4], # contact point for BASE
                                    msg_power.power, # power
                                    msg_power.energy # energy
                                    ])
            
             

    # initialize the buffer that will store all the information
    def start_new_simulation_try(self, world_name, dataset_type, i_sim, initial_pose):
        self.world_name_ = world_name
        self.dataset_type_ = dataset_type
        self.i_sim_ = i_sim
        self.initial_pose_ = initial_pose
        #
        self.data_buffer_np_ = []
        #self.data_buffer_ = self.data_buffer_.set_index(["TIMESTAMP"])
        self.current_twist_cmd_ = initial_fixed_twist
        self.set_flag_cmd_vel(True)
        self.publish_cmd_vel()
        #self.starting_time_ = rospy.Time.now() # do this in the callback to avoid an offset
        #self.start_shuffle_time_ = self.starting_time_
        self.counter_callback_calls_ = 0
        self.counter_shuffle_its_ = 0
        self.set_flag_process_inputs(True)

     
    # save the buffer to a csv and stop the try simulation
    def stop_simulation_try(self):
        # I_RIP imageframe robot initial position, S_ simulation frame, RCP robot current position, TLV twist linear vel, S_RCO_A simulation robot current orientation alpha/beta/gamma (orientation is given in euler angles
        # in the future we could add information regarding wheels speed, torque, etc
        columns = [ "TIMESTAMP",
                    "ROS_TIMESTAMP",
                    "I_RIP_X",
                    "I_RIP_Y",
                    "S_RIP_X",
                    "S_RIP_Y",
                    "S_RIP_Z",
                    "I_RCP_X", 
                    "I_RCP_Y",
                    "S_RCP_X", # position from odom (or fake odom)
                    "S_RCP_Y", 
                    "S_RCP_Z",
                    "S_RCO_A", # orientation from odom (or fake odom) (in euler angles)
                    "S_RCO_B",
                    "S_RCO_G",
                    "S_RC_T_STEER", # target steer (angular v.z)
                    "S_RC_T_LV_X", # target speed (linear v.z)
                    "S_RC_TLV_X", # these are cmd_val linear from Odometry
                    "S_RC_TLV_Y",
                    "S_RC_TLV_Z",
                    "S_RC_TAV_X", # these are cmd_val angular from Odometry
                    "S_RC_TAV_Y",
                    "S_RC_TAV_Z",
                    "S_RCS_FLW", # contact points data from each wheel and base link
                    "S_RCS_FRW",
                    "S_RCS_BLW",
                    "S_RCS_BRW",
                    "S_RCS_BL",
                    "S_RPW", # power
                    "S_REN" # energy
                   ]
        #self.data_buffer_ = pd.DataFrame(columns = columns)
        #self.data_buffer_ = self.data_buffer_.set_index("TIMESTAMP")
        rospy.loginfo("callback calls for current run: %d  ;  n/duration(s) %f", self.counter_callback_calls_, (self.counter_callback_calls_/20))
        if len(self.data_buffer_np_)>0:
            # only write files with data (no the one discarded due to out-of-map constrain
            self.data_buffer_np_= np.array(self.data_buffer_np_)
            self.data_buffer_ = pd.DataFrame(data=self.data_buffer_np_,columns = columns)
            rospy.loginfo("saving simulation data to %s",output_folder+self.world_name_+"_"+self.dataset_type_+"_"+str(self.i_sim_)+".csv")
            self.data_buffer_.to_csv(output_folder+self.world_name_+"_"+self.dataset_type_+"_"+str(self.i_sim_)+".csv")            
            return self.world_name_+"_"+self.dataset_type_+"_"+str(self.i_sim_)+".csv"
        else:
            return "void"
    
if __name__ == "__main__":
    if len(sys.argv) >= 4:
        world_name = sys.argv[1]
        dataset_type = sys.argv[2]
        number_tries = int(sys.argv[3])
    else:
        rospy.logerr(usage())
        sys.exit(1)
    rospy.init_node('run_simulation_manager')#, log_level=rospy.DEBUG)
    # we wait until gazebo is up and our robot-s model is running
    while get_model_state().success==False:
        rospy.loginfo("Waiting for %s model to be up in gazebo", model_name)
        rospy.sleep(1.)
    pubssubs_manager = PubsSubsManager()
    meta_csv_buffer = pd.DataFrame(columns = ["CSV","HEIGHTMAP"])
    for i_sim in range(0, number_tries):
        rospy.loginfo("=== Simulation %s/%s for map %s", str(i_sim), str(number_tries), world_name)
        random_model_state = generate_random_model_state()
        rospy.loginfo("-- spawning robot at %s", random_model_state.pose)
        #print "-- spawning robot at ", random_model_state.pose
        res = set_model_state(random_model_state)
        #print "-- ",res
        rospy.loginfo("-- %s", res)
        rospy.sleep(1.0)
        rospy.loginfo("-- listening for %s s", str(sim_duration.to_sec()) )
        pubssubs_manager.start_new_simulation_try(world_name, dataset_type, i_sim, random_model_state.pose)
        #start_t = rospy.Time.now()
        # listening for fixed duration
        #print "-- listening for ",sim_duration.to_sec()," s"
        while pubssubs_manager.get_flag_process_inputs():
            True;
            #current_t = rospy.Time.now()
            #if (current_t - start_t) >= sim_duration:
            #    pubssubs_manager.set_flag_process_inputs(False)
            #rospy.sleep(0.1)
        cvs_file_name = pubssubs_manager.stop_simulation_try()
        if cvs_file_name != "void":
            meta_csv_buffer.loc[len(meta_csv_buffer)] = [ cvs_file_name, world_name+".png"]
    meta_csv_buffer.to_csv(output_folder+"meta_"+world_name+"_"+dataset_type+".csv")
        
